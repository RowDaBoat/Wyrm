
all: release

release: bootloader releasekernel userland image

debug: bootloader debugkernel image

bootloader:
	cd Bootloader; make all

releasekernel:
	cd Kernel; make release

debugkernel:
	cd Kernel; make debug

userland:
	cd Userland; make all

image: kernel bootloader userland
	cd Image; make all

clean:
	cd Bootloader; make clean
	cd Image; make clean
	cd Kernel; make clean
	cd Userland; make clean

.PHONY: bootloader image collections kernel userland all clean
