#include "InterruptDescriptor.h"
#include "../Log/Log.h"

InterruptDescriptor::InterruptDescriptor() {
	lowBits = 0;
	segmentSelector = 0;
	reservedField0 = 0;
	typeAndAttributes = 0;
	middleBits = 0;
	highBits = 0;
	reservedField1 = 0;
}

InterruptDescriptor & InterruptDescriptor::setHandler(InterruptHandler handler) {
	highBits = ((uint64_t)handler & 0xFFFFFFFF00000000) >> 32;
	middleBits = ((uint64_t)handler & 0xFFFF0000) >> 16;
	lowBits = (uint64_t)handler & 0xFFFF;
	return *this;
}

InterruptDescriptor & InterruptDescriptor::setSegmentSelector(uint16_t value) {
	segmentSelector = value;
	return *this;
}

InterruptDescriptor & InterruptDescriptor::setPresent(bool value) {
	typeAndAttributes &= ~(1 << 7);
	typeAndAttributes |= (value << 7);
	return *this;
}

InterruptDescriptor & InterruptDescriptor::setPrivilegeLevel(PrivilegeLevel value) {
	typeAndAttributes &= ~(3 << 5);
	typeAndAttributes |= (value << 5);
	return *this;
}

InterruptDescriptor & InterruptDescriptor::setStorageSegment(bool value) {
	typeAndAttributes &= ~(1 << 4);
	typeAndAttributes |= (value << 4);
	return *this;
}

InterruptDescriptor & InterruptDescriptor::setGateType(GateType value) {
	typeAndAttributes &= ~0xF;
	typeAndAttributes |= value;
	return *this;
}

InterruptHandler InterruptDescriptor::getHandler() {
	return InterruptHandler(
		((uint64_t)highBits << 32) |
		((uint64_t)middleBits << 16) |
		(uint64_t)lowBits
	);
}

uint16_t InterruptDescriptor::getSegmentSelector() {
	return segmentSelector;
}

bool InterruptDescriptor::getPresent() {
	return (typeAndAttributes & (1 << 7)) != 0;
}

PrivilegeLevel InterruptDescriptor::getPrivilegeLevel() {
	return (PrivilegeLevel)((typeAndAttributes & (3 << 5)) >> 5);
}

bool InterruptDescriptor::getStorageSegment() {
	return (typeAndAttributes & (1 << 4)) != 0;
}

GateType InterruptDescriptor::getGateType() {
	return (GateType)(typeAndAttributes & 0xF);
}

void InterruptDescriptor::log(Log & log) {
	log.str("Hex: ").hex(*((uint64_t*)this + 1), 16).hex(*((uint64_t*)this), 16).newline();
	log.str("Handler: ").addr((void*)getHandler()).newline();
	log.str("Segment Selector: ").hex(getSegmentSelector(), 4).newline();
	log.str("Present: ").chr(getPresent()? '1' : '0').newline();
	log.str("Privilege: ").dec(getPrivilegeLevel()).newline();
	log.str("Storage Segment: ").chr(getStorageSegment()? '1' : '0').newline();
	log.str("Gate Type: ").hex(getGateType(), 1).newline();
}
