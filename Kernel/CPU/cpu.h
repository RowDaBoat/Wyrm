#ifndef CPU_H
#define CPU_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void writeCR0(uint64_t value);
uint64_t readCR0();
void writeCR2(uint64_t value);
uint64_t readCR2();
void writeCR3(uint64_t value);
uint64_t readCR3();
void writeCR4(uint64_t value);
uint64_t readCR4();
void outb(uint16_t port, uint8_t value);
void outd(uint16_t port, uint16_t value);
void outq(uint16_t port, uint32_t value);
uint8_t inb(uint16_t port);
uint16_t ind(uint16_t port);
uint32_t inq(uint16_t port);
void writeMSR(uint32_t registryId, uint64_t value);
void readMSR(uint32_t registryId, uint64_t * value);
void cpuid(uint32_t initialEax, uint32_t * eax, uint32_t * ebx,  uint32_t * ecx, uint32_t * edx);
void int80();
void disableInterrupts();
void enableInterrupts();
void invalidateVirtualAddress(void * virtualAddress);
void finishStartup();

#ifdef __cplusplus
}
#endif

#endif