#include <stdint.h>
#include "InterruptDescriptor.h"

typedef void (*InterruptHandler)();

//TODO: Implement with LAPIC instead of PIC
class InterruptTable {
private:
	static const uint8_t PIC1Command =	0x20;
	static const uint8_t PIC1Data =		0x21;

	static const uint8_t PIC2Command =	0xA0;
	static const uint8_t PIC2Data =		0xA1;

	static const uint8_t ICW1ICW4	=		0x01;	// ICW4 (not) needed
	static const uint8_t ICW1Single =		0x02;	// Single (cascade) mode
	static const uint8_t ICW1Interval4 =	0x04;	// Call address interval 4 (8)
	static const uint8_t ICW1Level =		0x08;	// Level triggered (edge) mode
	static const uint8_t ICW1Init =			0x10;	// Initialization - required!
 
	static const uint8_t ICW48086 =					0x01;	// 8086/88 (MCS-80/85) mode
	static const uint8_t ICW4Auto =					0x02;	// Auto (normal) EOI
	static const uint8_t ICW4BufferedSlave =		0x08;	// Buffered mode/slave
	static const uint8_t ICW4BufferedMaster =		0x0C;	// Buffered mode/master
	static const uint8_t ICW4SpecialFullyNested =	0x10;	// Special fully nested (not)

	uint8_t maskPIC1;
	uint8_t maskPIC2;
	InterruptDescriptor * idt;

public:
	InterruptTable(void * idt, uint16_t offsetPIC1, uint16_t offsetPIC2);
	InterruptTable & setHandler(uint32_t index, InterruptHandler handler);
	InterruptTable & setIRQMask(uint8_t index, bool value);
	InterruptHandler getHandler(uint32_t index);
	bool getIRQMask(uint8_t index);

private:
	void RemapPIC(uint16_t offsetPIC1, uint16_t offsetPIC2);
};
