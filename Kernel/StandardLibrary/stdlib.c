#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdint.h>

static void swap(void * e1, void * e2, uint64_t size);
static uint64_t partition(void * base, uint64_t low, uint64_t high, uint64_t size, int32_t (*comparer)(const void*,const void*));
static void quicksort(void * base, uint64_t low, uint64_t high, uint64_t size, int32_t (*comparer)(const void *, const void *));

void qsort (void * base, uint64_t num, uint64_t size, int32_t (*comparer)(const void *, const void *)) {
	quicksort(base, 0, num - 1, size, comparer);
}

static void quicksort(void * base, uint64_t low, uint64_t high, uint64_t size, int32_t (*comparer)(const void *, const void *)) {
	if (low < high) {
		uint64_t pivot = partition(base, low, high, size, comparer);
		quicksort(base, low, pivot == 0? 0 : pivot - 1, size, comparer);
		quicksort(base, pivot + 1, high, size, comparer);
	}
}

static uint64_t partition(void * base, uint64_t low, uint64_t high, uint64_t size, int32_t (*comparer)(const void*,const void*)) {
	uint64_t pivotIndex = low + (high - low) / 2;
	uint64_t storeIndex = low;
	uint8_t pivot[size];

	memcpy(pivot, (uint8_t*)base + pivotIndex * size, size);
	
	swap((uint8_t*)base + pivotIndex * size, (uint8_t*)base + high * size, size);

	for (int i = low; i < high; i++) {
		if (comparer((uint8_t*)base + i * size, pivot)) {
			swap((uint8_t*)base + i * size, (uint8_t*)base + storeIndex * size, size);
			storeIndex++;
		}
	}

	swap((uint8_t*)base + high * size, (uint8_t*)base + storeIndex * size, size);
	return storeIndex;
}

static void swap(void * e1, void * e2, uint64_t size) {
	uint8_t buffer[size];
	memcpy(buffer, e1, size);
	memcpy(e1, e2, size);
	memcpy(e2, buffer, size);
}

void * memcpy(void * dst, const void * src, uint64_t len) {
	/*
	* memcpy does not support overlapping buffers, so always do it
	* forwards. (Don't change this without adjusting memmove.)
	*
	* For speedy copying, optimize the common case where both pointers
	* and the length are word-aligned, and copy word-at-a-time instead
	* of byte-at-a-time. Otherwise, copy by bytes.
	*
	* The alignment logic below should be portable. We rely on
	* the compiler to be reasonably intelligent about optimizing
	* the divides and modulos out. Fortunately, it is.
	*/
	uint64_t i;

	if ((uint64_t)dst % sizeof(uint32_t) == 0 &&
		(uint64_t)src % sizeof(uint32_t) == 0 &&
		len % sizeof(uint32_t) == 0) {

		uint32_t *d = (uint32_t *) dst;
		const uint32_t *s = (const uint32_t *)src;

		for (i=0; i<len/sizeof(uint32_t); i++) {
			d[i] = s[i];
		}
	} else {
		uint8_t * d = (uint8_t*)dst;
		const uint8_t * s = (const uint8_t*)src;

		for (i=0; i<len; i++) {
			d[i] = s[i];
		}
	}

	return dst;
}

void * memset(void * destination, int32_t c, uint64_t length) {
	uint8_t chr = (uint8_t)c;
	char * dst = (char*)destination;

	while(length--)
		dst[length] = chr;

	return destination;
}
