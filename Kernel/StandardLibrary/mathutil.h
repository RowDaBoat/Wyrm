#ifndef MATHUTIL_H
#define MATHUTIL_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint64_t upperDivide(uint64_t n, uint64_t d);
uint32_t nextHighestPowerOf2(uint32_t n);
uint64_t max(uint64_t e1, uint64_t e2);
uint64_t min(uint64_t e1, uint64_t e2);

#ifdef __cplusplus
}
#endif

#endif