#ifndef PROCESS_H
#define PROCESS_H

#include "../MemoryManager/PageAllocator.h"

class Process {
public:
	void * userStack;
	void * kernelStack;
	void * entryPoint;

private:
	void * userStackPage;
	void * kernelStackPage;
	PageAllocator & pageAllocator;
	Log & log;

public:
	Process(void * entryPoint, PageAllocator & pageAllocator, Log & log);
	~Process();

private:
	static void * toStackAddress(void * page);
	static void * fillStackFrame(void * entryPoint, void * userStack);
};

#endif