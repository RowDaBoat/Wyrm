#ifndef TIMER_H
#define TIMER_H

class Timer {
public:
	Timer(Log & log);

private:
	bool cpuHasAPIC();
};

#endif