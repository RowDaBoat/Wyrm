#include "ProcessSlot.h"

ProcessSlot::ProcessSlot(Process * process) {
	this->process = process;
}

ProcessSlot::~ProcessSlot() {
	delete process;
}
