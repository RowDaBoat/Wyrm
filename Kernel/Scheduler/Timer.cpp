#include "../Log/Log.h"
#include "../CPU/cpu.h"
#include "Timer.h"

#define CPUID_FLAG_APIC (1 << 9	)
#define IA32_APIC_BASE_MSR 0x1B
#define IA32_APIC_BASE_MSR_ENABLE 0x800

Timer::Timer(Log & log) {
	log.str("Timer:").newline();
	log.str("Local APIC support: ").str(
		cpuHasAPIC()? "yes" : "no" 
	).newline();
}

bool Timer::cpuHasAPIC() {
	uint32_t eax, ebx, ecx, edx;
	cpuid(1, &eax, &ebx, &ecx, &edx);
	return edx & CPUID_FLAG_APIC;
}

// /* Set the physical address for local APIC registers */
// void cpuSetAPICBase(uint64_t * apic) {
// 	uint64_t reg = ((uint64_t)apic & 0xFFFFFF100) | IA32_APIC_BASE_MSR_ENABLE;
// 	writeMSR(IA32_APIC_BASE_MSR, reg);
// }
 
// // void readMSR(uint32_t registryId, uint64_t * value)
// /**
//  * Get the physical address of the APIC registers page
//  * make sure you map it to virtual memory ;)
//  */
// uint64_t * cpuGetAPICBase()
// {
// 	uint64_t reg;
// 	readMSR(IA32_APIC_BASE_MSR, &reg);
// 	return (uint64_t*)(reg & 0xFFFFFF100);
// }
 
// void enableAPIC()
// {
// 	/* Hardware enable the Local APIC if it wasn't enabled */
// 	cpuSetAPICBase(cpuGetAPICBase());

// 	/* Set the Spourious Interrupt Vector Register bit 8 to start receiving interrupts */
// 	//WriteRegister(0xF0, ReadRegister(0xF0) | 0x100);
// }