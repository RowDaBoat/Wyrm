#ifndef PROCESS_SLOT_H
#define PROCESS_SLOT_H

#include "Process.h"

class ProcessSlot {
public:
	ProcessSlot * next;
	Process * process;

	ProcessSlot(Process * process);
	~ProcessSlot();
};

#endif