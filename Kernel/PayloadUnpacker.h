#ifndef PROCESS_LOADER_H
#define PROCESS_LOADER_H

#include "MemoryManager/PageAllocator.h"
#include "Log/Log.h"

class PayloadUnpacker {
private:
	 PageAllocator & pageAllocator;
	 Log & log;
	uint64_t processCount;
	void * readPointer;

public:
	PayloadUnpacker(const void * packedPayloadAddress, PageAllocator & pageAllocator, Log & log);
	void * loadNext(uint32_t & size);

private:
	uint32_t readUint32(void *& readPointer);
	void readChunk(void * destination, uint32_t size, void *& readPointer);
};

#endif