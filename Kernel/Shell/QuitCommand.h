#ifndef QUIT_COMMAND_H
#define QUIT_COMMAND_H

#include "BaseShell.h"
#include "Command.h"

using namespace std;

class QuitCommand : public Command {
private:
	BaseShell & baseShell;
public:
	QuitCommand(BaseShell & BaseShell);

	string getName();
	string getCommand();
	string getHelp();
	void execute(CommandArguments & arguments);
};

#endif