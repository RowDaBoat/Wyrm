#include <string>
#include "QuitCommand.h"
#include "BaseShell.h"

HelpCommand::HelpCommand(vector<Command*> * commands) : Command("help", "h", "print this help.") {
	this->commands = commands;
}

void HelpCommand::execute(CommandArguments & arguments) {
	arguments.getSuccess();

	for (int i = 0; i < commands->size(); i++)
		cout << getCommandHelp((*commands)[i]) << endl;
}

string HelpCommand::getCommandHelp(Command * command) {
	return command->getCommand() + " '" + command->getName() + "': " + command->getHelp();
}
