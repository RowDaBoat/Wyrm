#include <stdlib.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include "BaseShell.h"
#include "Command.h"
#include "HelpCommand.h"
#include "QuitCommand.h"

using namespace std;

void BaseShell::run(vector<Command*> * commands) {
	running = true;
	HelpCommand help(commands);
	QuitCommand quit(*this);

	commands->push_back(&help);
	commands->push_back(&quit);

	while(running) {
		cout << getShellName() << "> ";
		parseCommand(commands, &help);
	}
}

void BaseShell::beforeCommandExecution() {
}

void BaseShell::afterCommandExecution() {
}

bool BaseShell::quit() {
	running = false;
}

void BaseShell::parseCommand(vector<Command*> * commands, HelpCommand * help) {
	bool executed = false;

	string command;
	cin >> command;

	for (int i = 0; i < commands->size(); i++) {
		Command * current = (*commands)[i];

		if (current->getCommand().compare(command) == 0) {
			beforeCommandExecution();
			CommandArguments arg(help->getCommandHelp(current));
			current->execute(arg);
			executed = true;
			afterCommandExecution();
		}
	}

	if (!executed) {
		cerr << "Invalid command." << endl << endl;

		for (int i = 0; i < commands->size(); i++)
			cerr << help->getCommandHelp((*commands)[i]) << endl;
	}
}
