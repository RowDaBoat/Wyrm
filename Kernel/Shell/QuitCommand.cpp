#include <string>
#include "QuitCommand.h"
#include "BaseShell.h"

QuitCommand::QuitCommand(BaseShell & b) : Command("quit", "q", "quit the application."), baseShell(b) {
}

void QuitCommand::execute(CommandArguments & arguments) {
	arguments.getSuccess();
	baseShell.quit();
}
