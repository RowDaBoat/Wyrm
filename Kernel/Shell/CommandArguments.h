#ifndef COMMAND_ARGUMENTS_H
#define COMMAND_ARGUMENTS_H

#include <iostream>
#include <string>
#include <stdio.h>
#include <sstream>

using namespace std;

class CommandArguments {
private:
	string errorMessage;
	bool success;
	istringstream * inputStream;

public:
	CommandArguments(string errorMessage) {
		success = true;

		string line;
		getline(cin, line);
		inputStream = new istringstream(line);
	}

	template<typename T>
	CommandArguments & get(T & value) {
		*inputStream >> value;
		
		if (inputStream->fail()) {
			success = false;
			cerr << errorMessage;
		}

		return *this;
	}

	bool isAtEnd() {
		*inputStream >> ws;
		return inputStream->eof();
	}

	bool getSuccess() {
		return success;
	}
};

#endif