#ifndef PAGINGUTILITY_H
#define PAGINGUTILITY_H

#include <stdint.h>

class PagingUtility {
public:
	static const uint16_t pml4Bits =					39;
	static const uint16_t directoryPointerTableBits =	30;
	static const uint16_t directoryBits =				21;
	static const uint16_t tableBits =					12;

	static const uint64_t pml4Mask =					0x0000FF8000000000;
	static const uint64_t directoryPointerTableMask =	0x0000007FC0000000;
	static const uint64_t directoryMask =				0x000000003FE00000;
	static const uint64_t tableMask =					0x00000000001FF000;
	static const uint64_t offsetMask = 					0x0000000000000FFF;
	static const uint64_t addressMask4KiB =				0xFFFFFFFFFFFFF000;
	static const uint64_t addressMask2MiB =				0xFFFFFFFFFFE00000;
	static const uint64_t addressMask1GiB =				0xFFFFFFFFC0000000;

	inline static uint16_t getPML4Entry(void * virtualAddress) {
		return (uint16_t)(((uint64_t)virtualAddress & pml4Mask) >> pml4Bits);
	}

	inline static uint16_t getPageDirectoryPointerTableEntry(void * virtualAddress){
		return (uint16_t)(((uint64_t)virtualAddress & directoryPointerTableMask) >> directoryPointerTableBits);
	}

	inline static uint16_t getPageDirectoryEntry(void * virtualAddress) {
		return (uint16_t)(((uint64_t)virtualAddress & directoryMask) >> directoryBits);
	}

	inline static uint16_t getPageTableEntry(void * virtualAddress) {
		return (uint16_t)(((uint64_t)virtualAddress & tableMask) >> tableBits);
	}

	inline static uint16_t getOffset(void * virtualAddress) {
		return (uint16_t)((uint64_t)virtualAddress & offsetMask);
	}

	inline static bool is4KiBAligned(void * address) {
		return ((uint64_t)address & ~addressMask4KiB) == 0;
	}

	inline static bool is2MiBAligned(void * address) {
		return ((uint64_t)address & ~addressMask2MiB) == 0;
	}

	inline static bool is1GiBAligned(void * address) {
		return ((uint64_t)address & ~addressMask1GiB) == 0;
	}
};

#endif
