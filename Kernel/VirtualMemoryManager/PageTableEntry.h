#ifndef PAGEENTRY_H
#define PAGEENTRY_H

#include <stdint.h>

struct PageTableEntry {
	PageTableEntry() {
		contents = 0;
	}

	PageTableEntry(uint64_t c) {
		contents = c;
	}

	inline PageTableEntry & setPresent(bool value) {
		setBit(contents, presentBit, value);
		return *this;
	}

	inline PageTableEntry & setWritable(bool value) {
		setBit(contents, writeBit, value);
		return *this;
	}

	inline PageTableEntry & setUser(bool value) {
		setBit(contents, userBit, value);
		return *this;
	}

	inline PageTableEntry & setPageWriteThrough(bool value) {
		setBit(contents, pageWriteThroughBit, value);
		return *this;
	}

	inline PageTableEntry & setPageCacheDisable(bool value) {
		setBit(contents, pageCacheDisableBit, value);
		return *this;
	}

	inline PageTableEntry & setAccessed(bool value) {
		setBit(contents, accessedBit, value);
		return *this;
	}

	inline PageTableEntry & setDirty(bool value) {
		setBit(contents, dirtyBit, value);
		return *this;
	}

	inline PageTableEntry & setPageSize(bool value) {
		setBit(contents, pageSizeBit, value);
		return *this;
	}

	inline PageTableEntry & setGlobal(bool value) {
		setBit(contents, globalBit, value);
		return *this;
	}

	inline PageTableEntry & setPageAttribueTable(bool value) {
		setBit(contents, pageAttributeTableBit, value);
		return *this;
	}

	inline PageTableEntry & set4KiBPageAddress(void * address) {
		contents = ((uint64_t)address & addressMask4KiB) | (contents & ~addressMask4KiB);
		return *this;
	}

	inline PageTableEntry & set2MiBPageAddress(void * address) {
		contents = ((uint64_t)address & addressMask2MiB) | (contents & ~addressMask2MiB);
		return *this;
	}

	inline PageTableEntry & set1GiBPageAddress(void * address) {
		contents = ((uint64_t)address & addressMask1GiB) | (contents & ~addressMask1GiB);
		return *this;
	}

	inline PageTableEntry & setExecuteDisable(bool value) {
		setBit(contents, executeDisableBit, value);
		return *this;
	}

	inline bool getPresent() {
		return getBit(contents, presentBit);
	}

	inline bool getWritable() {
		return getBit(contents, writeBit);
	}

	inline bool getUser() {
		return getBit(contents, userBit);
	}

	inline bool getPageWriteThrough() {
		return getBit(contents, pageWriteThroughBit);
	}

	inline bool getPageCacheDisable() {
		return getBit(contents, pageCacheDisableBit);
	}

	inline bool getAccessed() {
		return getBit(contents, accessedBit);
	}

	inline bool getDirty() {
		return getBit(contents, dirtyBit);
	}

	inline bool getPageSize() {
		return getBit(contents, pageSizeBit);
	}

	inline bool getGlobal() {
		return getBit(contents, globalBit);
	}

	inline bool getPageAttribueTable() {
		return getBit(contents, pageAttributeTableBit);
	}

	inline void * get4KiBPageAddress() {
		return (void*)(contents & addressMask4KiB);
	}

	inline void * get2MiBPageAddress() {
		return (void*)(contents & addressMask2MiB);
	}

	inline void * get1GiBPageAddress() {
		return (void*)(contents & addressMask1GiB);
	}

	inline bool getExecuteDisable(bool value) {
		return getBit(contents, executeDisableBit);
	}

	static inline PageTableEntry * clear(PageTableEntry * page) {
		for (uint64_t i = 0; i < pageSize / sizeof(PageTableEntry); i++)
			page[i].contents = 0;

		return page;
	}

private:
	//TODO: get MAXPHYSADDR from CPUID
	static const uint32_t presentBit = 0;
	static const uint32_t writeBit = 1;
	static const uint32_t userBit = 2;
	static const uint32_t pageWriteThroughBit = 3;
	static const uint32_t pageCacheDisableBit = 4;
	static const uint32_t accessedBit = 5;
	static const uint32_t dirtyBit = 6;
	static const uint32_t pageSizeBit = 7;
	static const uint32_t globalBit = 8;
	static const uint32_t pageAttributeTableBit = 12;
	static const uint32_t addressBit = 30;
	static const uint64_t addressMask4KiB = 0x0000000FFFFFFFFF000;
	static const uint64_t addressMask2MiB = 0x0000000FFFFFFE00000;
	static const uint64_t addressMask1GiB = 0x0000000FFFFC0000000;
	static const uint32_t executeDisableBit = 63;
	static const uint64_t pageSize = 4092;

	uint64_t contents;

	static inline void setBit(uint64_t & bits, uint32_t n, bool value) {
		if (value)
			bits |= (1 << n);
		else
			bits &= ~(1 << n);
	}

	static inline bool getBit(uint64_t bits, uint32_t n) {
		return bits & (1 << n);
	}
};

#endif