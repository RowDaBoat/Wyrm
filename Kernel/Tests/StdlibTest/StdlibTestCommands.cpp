#include <stdlib.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include "StdlibTestCommands.h"
#include "../../Shell/CommandArguments.h"
#include "../../Shell/Command.h"

int intComp(int * e1, int * e2) {
	return *e1 - *e2;
}

SortCommand::SortCommand() : Command("sort", "s", "usage: s <e0> <e1> ... <eN>") {
}

void SortCommand::execute(CommandArguments & arguments) {
	int * array = NULL;
	int count = 0;

	while(!arguments.isAtEnd() && arguments.getSuccess()) {
		int argument = 0;
		arguments.get(argument);
		array = (int*)realloc(array, ++count * sizeof(int));
		array[count - 1] = argument;
	}

	if (!arguments.getSuccess())
		return;

	for (int i = 0; i < count; i++) {
		cout << array[i] << ' ';
	}

	cout << endl;

	qsort(array, count, sizeof(int), (__compar_fn_t)intComp);

	for (int i = 0; i < count; i++) {
		cout << array[i] << ' ';
	}

	cout << endl;
}
