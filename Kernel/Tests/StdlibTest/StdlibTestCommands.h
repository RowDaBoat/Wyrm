#ifndef STDLIB_TEST_COMMANDS_H
#define STDLIB_TEST_COMMANDS_H

#include "../../Shell/CommandArguments.h"
#include "../../Shell/Command.h"

int intComp(int * e1, int * e2);

class SortCommand : public Command {
public:
	SortCommand();
	void execute(CommandArguments & arguments);
};

#endif