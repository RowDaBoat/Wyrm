#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include "Test.h"
#include "ListTestCommands.h"
#include "../../Shell/BaseShell.h"
#include "../../Log/TestLog.h"

using namespace std;

Test::Test() {
	list = NULL;
	newList();
	printList = false;
	reversePrintList = false;
}

string Test::getShellName() {
	return "list";
}

void Test::run() {
	vector<Command*> commands;

	NewListCommand newList(*this);
	PushBackCommand pushBack(*this);
	PushFrontCommand pushFront(*this);
	PopBackCommand popBack(*this);
	PopFrontCommand popFront(*this);	
	ClearCommand clear(*this);
	IteratorPrevCommand prev(*this);
	IteratorNextCommand next(*this);
	PrintCommand togglePrint(*this);
	ReversePrintCommand reversePrint(*this);
	InsertCommand insert(*this);
	RemoveCommand remove(*this);

	commands.push_back(&newList);
	commands.push_back(&pushBack);
	commands.push_back(&pushFront);
	commands.push_back(&popBack);
	commands.push_back(&popFront);
	commands.push_back(&clear);
	commands.push_back(&prev);
	commands.push_back(&next);
	commands.push_back(&insert);
	commands.push_back(&remove);
	commands.push_back(&togglePrint);
	commands.push_back(&reversePrint);

	BaseShell::run(&commands);
}

void Test::newList() {
	if (list != NULL) {
		delete list;
	} 

	list = new List<int>();
	iterator = list->begin();
}

void Test::pushBack(int value) {
	list->pushBack(value);
}

void Test::pushFront(int value) {
	list->pushFront(value);
}

void Test::popBack() {
	log.str("Will pop: ").dec(list->back()).newline();
	list->popBack();
}

void Test::popFront() {
	log.str("Will pop: ").dec(list->front()).newline();
	list->popFront();
}

void Test::togglePrint() {
	printList = !printList;
}

void Test::toggleReversePrint() {
	reversePrintList = !reversePrintList;
}

void Test::clear() {
	list->clear();
}

void Test::iteratorNext() {
	if (!iterator.atEnd())
		iterator++;
}

void Test::iteratorPrev() {
	if (!iterator.atBeginning())
		iterator--;
}

void Test::insert(int element) {
	iterator.insert(element);
}

void Test::remove() {
	if (!iterator.atEnd())
		iterator.remove();
}

void Test::afterCommandExecution() {
	if (printList && !reversePrintList) {
		for(List<int>::Iterator i = list->begin(); !i.atEnd(); ++i) {
			bool thisIsIt = (i == iterator);
			log.str(thisIsIt? "[" : "").dec(*i).str(thisIsIt? "]" : "").str(", ");
		}

		if (iterator == list->end())
			log.str(" []");

		log.newline();
	} else if (printList) {
		if (iterator == list->end())
			log.str("[] ");

		for(List<int>::Iterator i = list->end(); !i.atBeginning();) {
			--i;
			bool thisIsIt = (i == iterator);
			log.str(thisIsIt? "[" : "").dec(*i).str(thisIsIt? "]" : "").str(", ");
		}

		log.str("(Reversed)").newline();
	}

	log.dec(list->count()).str(" items").newline();
}

int main(int argc, char ** argv) {
	Test test;
	test.run();
	return 0;
}