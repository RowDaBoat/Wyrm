#ifndef LISTTESTCOMANDS_H
#define LISTTESTCOMANDS_H

#include <string>
#include <iostream>
#include "Test.h"
#include "../../Shell/Command.h"
#include "../../Collections/List.h"

class NewListCommand : public Command {
	Test & test;

public:
	NewListCommand(Test & t);
	void execute(CommandArguments & arguments);
};

class PrintCommand : public Command {
	Test & test;

public:
	PrintCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class ReversePrintCommand : public  Command {
	Test & test;

public:
	ReversePrintCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class PushBackCommand : public Command {
	Test & test;

public:
	PushBackCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class PushFrontCommand : public Command {
	Test & test;

public:
	PushFrontCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class PopBackCommand : public Command {
	Test & test;

public:
	PopBackCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class PopFrontCommand : public Command {
	Test & test;

public:
	PopFrontCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class ClearCommand : public Command {
	Test & test;

public:
	ClearCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class IteratorNextCommand : public Command{
	Test & test;

public:
	IteratorNextCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class IteratorPrevCommand : public Command {
	Test & test;

public:
	IteratorPrevCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class InsertCommand : public Command{
	Test & test;

public:
	InsertCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class RemoveCommand : public Command {
	Test & test;

public:
	RemoveCommand(Test & test);
	void execute(CommandArguments & arguments);
};


#endif