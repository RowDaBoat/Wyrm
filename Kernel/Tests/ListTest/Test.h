#ifndef TEST_H
#define TEST_H

#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include "Test.h"
#include "../../Shell/BaseShell.h"
#include "../../Collections/List.h"
#include "../../Log/TestLog.h"

using namespace std;

class Test : BaseShell {
private:
	List<int> * list;
	List<int>::Iterator iterator;
	TestLog log;
	bool printList;
	bool reversePrintList;

public:
	Test();
	string getShellName();
	void run();
	void newList();
	void pushBack(int value);
	void pushFront(int value);
	void popBack();
	void popFront();
	void togglePrint();
	void toggleReversePrint();
	void clear();
	void iteratorNext();
	void iteratorPrev();
	void insert(int element);
	void remove();
	virtual void afterCommandExecution();

private:
};

#endif