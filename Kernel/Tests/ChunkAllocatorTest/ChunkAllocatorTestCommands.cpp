#include <string>
#include <iostream>
#include <list>
#include "Test.h"
#include "ChunkAllocatorTestCommands.h"
#include "../../Shell/Command.h"
#include "../../MemoryManager/ChunkAllocator.h"

NewAllocatorCommand::NewAllocatorCommand(Test & t) :
	Command("new allocator", "n", "usage: n <page count> <chunk size>"),
	test(t) {
}

void NewAllocatorCommand::execute(CommandArguments & arguments) {
	int pageCount = 64;
	int chunkSize = 512;

	if (!arguments.get(pageCount).get(chunkSize).getSuccess())
		return;

	test.newAllocator(pageCount, chunkSize);
}

AllocCommand::AllocCommand(Test & t) :
	Command("allocate", "a", "usage: a"),
	test(t) {
}

void AllocCommand::execute(CommandArguments & arguments) {
	void * allocated = test.alloc();

	if (allocated != NULL)
		cout << "Allocated a chunk at " << allocated << endl;
	else
		cout << "Not enough memory to allocate a chunk" << endl;
}

FreeCommand::FreeCommand(Test & t)  :
	Command("free", "f", "usage: f <address>"),
	test(t) {
}

void FreeCommand::execute(CommandArguments & arguments) {
	list<void *> addresses;

	while(!arguments.isAtEnd()) {
		void * address = NULL;
		arguments.get(address);
		addresses.push_back(address);

		if (!arguments.getSuccess())
			return;
	}

	while (!addresses.empty()) {
		test.dealloc(addresses.front());
		addresses.pop_front();
	}
}

StressCommand::StressCommand(Test & t) :
	Command("stress test", "s", "usage: s <insertion count>"),
	test(t) {
}

void StressCommand::execute(CommandArguments & arguments) {
	int count = 0;
	int size = 0;

	if (!arguments.get(count).get(size).getSuccess())
		return;

	test.stressTest(count, size);
}

PrintTreeCommand::PrintTreeCommand(Test & t) :
	Command("toggle print tree", "p", "usage: n <page count> <chunk size>"),
	test(t) {

}

void PrintTreeCommand::execute(CommandArguments & arguments) {
	test.togglePrintTree();
}
