#include "RedBlackTreeTestCommands.h"
#include "Test.h"
#include "../../Shell/Command.h"

NewTreeCommand::NewTreeCommand(Test & t) :
	Command("new tree", "n", "usage: n"),
	test(t) {
}

void NewTreeCommand::execute(CommandArguments & arguments) {
	test.newTree();
}

InsertCommand::InsertCommand(Test & t) :
	Command("insert element", "i", "usage: i <element>"),
	test(t) {
}

void InsertCommand::execute(CommandArguments & arguments) {
	int value = 0;

	if (!arguments.get(value).getSuccess())
		return;

	test.insert(value);
}

RemoveCommand::RemoveCommand(Test & t) :
	Command("remove element", "r", "usage: r <element>"),
	test(t) {
}

void RemoveCommand::execute(CommandArguments & arguments) {
	int value = 0;

	if (!arguments.get(value).getSuccess())
		return;

	test.remove(value);
}

StressTestCommand::StressTestCommand(Test & t) :
	Command("stress test", "s", "usage: s <element count>"),
	test(t) {
}

void StressTestCommand::execute(CommandArguments & arguments) {
	int count = 0;

	if (!arguments.get(count).getSuccess())
		return;

	test.stressTest(count);
}

TogglePrintCommand::TogglePrintCommand(Test & t) :
	Command("toggle print tree", "p", "usage: p"),
	test(t) {
}

void TogglePrintCommand::execute(CommandArguments & arguments) {
	test.togglePrintTree();
}

MaxCommand::MaxCommand(Test & t) :
	Command("get the max node", "max", "usage: max"),
	test(t) {
}

void MaxCommand::execute(CommandArguments & arguments) {
	test.max();
}

MaxCapCommand::MaxCapCommand(Test & t) :
	Command("get the max node below a cap", "maxcap", "usage: maxcap <cap>"),
	test(t) {
}

void MaxCapCommand::execute(CommandArguments & arguments) {
	int cap = 0;

	if (!arguments.get(cap).getSuccess())
		return;

	test.max(cap);
}

MinCommand::MinCommand(Test & t) :
	Command("get the min node", "min", "usage: min"),
	test(t) {
}

void MinCommand::execute(CommandArguments & arguments) {
	test.min();
}

MinCapCommand::MinCapCommand(Test & t) :
	Command("get the min node above a cap", "mincap", "usage: mincap <cap>"),
	test(t) {
}

void MinCapCommand::execute(CommandArguments & arguments) {
	int cap = 0;

	if (!arguments.get(cap).getSuccess())
		return;

	test.min(cap);
}
