#include <string>
#include <iostream>
#include <list>
#include "Test.h"
#include "AllocatorSelectorTestCommands.h"
#include "../../Shell/Command.h"

NewAllocatorCommand::NewAllocatorCommand(Test & t) :
	Command("new allocator", "n", "usage: n <page count>"),
	test(t) {
}

void NewAllocatorCommand::execute(CommandArguments & arguments) {
	int pageCount = 512;

	if (!arguments.get(pageCount).getSuccess())
		return;

	test.newAllocator(pageCount);
}

AllocCommand::AllocCommand(Test & t) :
	Command("allocate", "a", "usage: a <size> <count>"),
	test(t) {
}

void AllocCommand::execute(CommandArguments & arguments) {
	int size = 0;
	int count = 0;

	if (!arguments.get(size).get(count).getSuccess())
		return;


	for (int i = 0; i < count; i++) {
		void * allocated = test.alloc(size);

		if (allocated != NULL)
			cout << "Allocated a chunk at " << allocated << endl;
		else
			cout << "Not enough memory to allocate a chunk" << endl;
	}
}

FreeCommand::FreeCommand(Test & t)  :
	Command("free", "f", "usage: f <address 0> <address 1> ... <address n>"),
	test(t) {
}

void FreeCommand::execute(CommandArguments & arguments) {
	list<void *> addresses;

	while(!arguments.isAtEnd()) {
		void * address = NULL;
		arguments.get(address);

		if (!arguments.getSuccess())
			return;

		addresses.push_back(address);
	}

	while (!addresses.empty()) {
		test.dealloc(addresses.front());
		addresses.pop_front();
	}
}

AllocDeallocCommand::AllocDeallocCommand(Test & t) :
	Command("allocate deallocate", "ad", "usage: ad <size> <count>"),
	test(t) {
}

void AllocDeallocCommand::execute(CommandArguments & arguments) {
	int size = 0;
	int count = 0;

	if (!arguments.get(size).get(count).getSuccess())
		return;


	for (int i = 0; i < count; i++) {
		void * allocated = test.alloc(size);

		if (allocated != NULL) {
			cout << "Allocated a chunk at " << allocated << endl;
			test.dealloc(allocated);
		} else {
			cout << "Not enough memory to allocate a chunk" << endl;
		}
	}
}