#include <stdint.h>
#include <iostream>
#include <string>
#include "TreePrinter.h"
#include "../../Collections/BitArray.h"
#include "../../MemoryManager/PageAllocator.h"

using namespace std;

TreePrinter::TreePrinter(PageAllocator & pa) : allocator(pa) {
}

void TreePrinter::printMap() {
	uint32_t pageCount = allocator.managedSize() / PageAllocator::PageSize;
	uint8_t * buddyTable = (uint8_t*)(allocator.managedMemoryStart());
	int currentBit = 0;
	int width = PageCharWidth * pageCount;
	int nodeSize = pageCount;
	int nodeCount = 1;

	cout << "00: Free" << endl;
	cout << "01: Partial" << endl;
	cout << "10: Complete" << endl;
	cout << "11: Requested" << endl;

	do {
		printNodeRow(buddyTable, currentBit, nodeCount, width);
		nodeCount *= 2;
		width /= 2;
	} while ((nodeSize /= 2) >= 1);
}

void TreePrinter::printNodeRow(uint8_t * table, int & current, int nodeCount, int nodeWidth) {
	for(int i = 0; i < nodeCount; i++) {
		printNode(
			nodeWidth,
			BitArray::get(table, current + 0),
			BitArray::get(table, current + 1)
		);

		current += 2;
	}

	cout << endl;
}

void TreePrinter::printNode(int width, bool partialOrRequested, bool complete) {
	string caption =
		!partialOrRequested && !complete? "00" :
		partialOrRequested && !complete? "01" :
		!partialOrRequested && complete? "10" : "11";

	int spaceCount = (width - 4) / 2;
	string spaces = "";

	while (spaceCount--)
		spaces += " ";

	cout << "[" << spaces << caption << spaces << "]";
}
