#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include "Test.h"
#include "PageAllocatorTestCommands.h"
#include "TreePrinter.h"
#include "../../Shell/BaseShell.h"
#include "../../Collections/BitArray.h"
#include "../../MemoryManager/PageAllocator.h"
#include "../../Log/TestLog.h"

using namespace std;

Test::Test() {
	allocator = NULL;
	treePrinter = NULL;
	newAllocator(16);
	printMapEnabled = false;
}

string Test::getShellName() {
	return "pageAllocator";
}

void Test::run() {
	vector<Command*> commands;

	NewAllocatorCommand newAllocator(*this);
	AllocCommand alloc(*this);
	AllocAtCommand allocAt(*this);
	FreeCommand free(*this);
	BlockCommand block(*this);
	PrintMapCommand printMap(*this);

	commands.push_back(&newAllocator);
	commands.push_back(&alloc);
	commands.push_back(&allocAt);
	commands.push_back(&free);
	commands.push_back(&block);
	commands.push_back(&printMap);

	BaseShell::run(&commands);
}

void Test::newAllocator(int managedMemoryInPages) {
	if (allocator != NULL) {
		free(allocator->managedMemoryStart());
		delete allocator;
		delete treePrinter;
	}

	void * mem = malloc(managedMemoryInPages * PageSize);

	if (mem == NULL) {
		cout << "Not enough memory" << endl;
		return;
	}

	allocator = new PageAllocator(mem, managedMemoryInPages * PageSize, log);
	treePrinter = new TreePrinter(*allocator);
}

void Test::afterCommandExecution() {
	if (printMapEnabled) {
		treePrinter->printMap();
	}
}

void * Test::alloc(int count) {
	allocator->alloc(count);
}

void * Test::alloc(void * address, int count) {
	allocator->alloc(address, count);
}

void Test::free(void * address) {
	allocator->dealloc(address);
}

void Test::blockSection(void * addressFrom, void * addressTo) {
	allocator->blockSection(addressFrom, addressTo);
}

void Test::togglePrintMap() {
	printMapEnabled = !printMapEnabled;
}

int main(int argc, char ** argv) {
	Test test;
	test.run();
	return 0;
}