#ifndef TREE_PRINTER_H
#define TREE_PRINTER_H

#include <stdint.h>
#include <iostream>
#include <string>
#include "../../Collections/BitArray.h"
#include "../../MemoryManager/PageAllocator.h"

class TreePrinter {
private:
	static const int PageCharWidth = 4;
	PageAllocator & allocator;

public:
	TreePrinter(PageAllocator & pageAllocator);
	void printMap();
	void printNodeRow(uint8_t * table, int & current, int nodeCount, int nodeWidth);
	void printNode(int width, bool partialOrRequested, bool complete);
};

#endif