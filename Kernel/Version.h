#ifndef VERSION_H
#define VERSION_H


struct Version {
	static const char * KernelName;
	static const char * ReleaseName;
	static const int Mayor;
	static const int Minor;
	static const char * Arch;
};

const char * Version::KernelName = "Wyrm";
const char * Version::ReleaseName = "Tiamat";
const int Version::Mayor = 0;
const int Version::Minor = 1;
const char * Version::Arch = "x86_64";

#endif