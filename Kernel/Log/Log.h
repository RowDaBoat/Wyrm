#ifndef LOG_H
#define LOG_H

#include <stdint.h>

typedef struct {
	char character;
	char attribute;
} VideoCell;

class Log {
private:
	char buf[128];

public:
	virtual Log& newline() = 0;
	virtual Log& tab() = 0;
	virtual Log& chr(int c) = 0;
	virtual Log& clear() = 0;

	Log& str(const char * string);
	Log& bits8(uint8_t value);
	Log& bits16(uint16_t value);
	Log& bits32(uint32_t value);
	Log& bits64(uint64_t value);
	Log& addr(void * value);
	Log& laddr(void * value);
	Log& haddr(void * value);
	Log& count(uint32_t value);
	Log& dec(int32_t value);
	Log& hex(uint64_t value, int digits);
	Log& sizeb(uint64_t value);
	Log& sizeKb(uint64_t value);
	Log& sizeMb(uint64_t value);
	Log& md5sum(void * address, uint64_t length);

protected:
	void printBase(uint32_t value, int completeTo, int base);
	int uintToBase(uint32_t value, char * buf, int base);
};

#endif