#include <stdint.h>
#include <stdio.h>
#include "TestLog.h"

using namespace std;

Log& TestLog::newline() {
	putchar('\n');
}

Log& TestLog::tab() {
	putchar('\t');
}

Log& TestLog::chr(int c) {
	putchar(c);
}

Log& TestLog::clear() {
	for (int i = 0; i < 25; i++)
		putchar('\n');
}
