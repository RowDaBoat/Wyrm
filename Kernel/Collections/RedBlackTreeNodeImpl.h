#ifndef REDBLACKTREENODEIMPL_H
#define REDBLACKTREENODEIMPL_H

#include <stddef.h>
#include "RedBlackTreeNode.h"

template<typename T>
class RedBlackTreeNodeImpl : public RedBlackTreeNode<T> {
public:
	RedBlackTreeNodeImpl(T & element);

	virtual RedBlackTreeNode<T> * getParent();
	virtual RedBlackTreeNode<T> * getLeftChild();
	virtual RedBlackTreeNode<T> * getRightChild();
	virtual RedBlackTreeNode<T> * getGrandparent();
	virtual RedBlackTreeNode<T> * getSibling();
	virtual RedBlackTreeNode<T> * getUncle();
	virtual RedBlackTreeNode<T> * inOrderPrev();
	virtual RedBlackTreeNode<T> * inOrderNext();
	virtual RedBlackTreeNode<T> * min();
	virtual RedBlackTreeNode<T> * min(T & cap);
	virtual RedBlackTreeNode<T> * max();
	virtual RedBlackTreeNode<T> * max(T & cap);
	virtual RedBlackTreeNode<T> * search(T & toSearch);
	virtual RedBlackTreeColor getColor();
	virtual T & getElement();

	T element;
	RedBlackTreeNodeImpl<T> * parent;
	RedBlackTreeNodeImpl<T> * left;
	RedBlackTreeNodeImpl<T> * right;
	RedBlackTreeColor color;
};

template<typename T>
RedBlackTreeNodeImpl<T>::RedBlackTreeNodeImpl(T & e) : element(e) {
	parent = NULL;
	left = NULL;
	right = NULL;
	color = RBTRed;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::getParent() {
	return parent;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::getLeftChild() {
	return left;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::getRightChild() {
	return right;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::getGrandparent() {
	return parent == NULL? NULL : parent->parent;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::getSibling() {
	return parent == NULL? NULL :
		parent->left == this?
			parent->right :
			parent->left;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::getUncle() {
	RedBlackTreeNodeImpl<T> * grandParent = (RedBlackTreeNodeImpl<T>*)getGrandparent();

	return grandParent == NULL?
		NULL :
		grandParent->left == parent?
			grandParent->right :
			grandParent->left;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::inOrderPrev() {
	if (left != NULL)
		return left->max();

	RedBlackTreeNodeImpl<T> * current = this;
	RedBlackTreeNodeImpl<T> * currentsParent = (RedBlackTreeNodeImpl<T>*)parent;

	while(currentsParent != NULL && current == currentsParent->left) {
		current = currentsParent;
		currentsParent = currentsParent->parent;
	}

	return currentsParent;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::inOrderNext() {
	if (right != NULL)
		return right->min();

	RedBlackTreeNodeImpl<T> * current = this;
	RedBlackTreeNodeImpl<T> * currentsParent = (RedBlackTreeNodeImpl<T>*)parent;

	while(currentsParent != NULL && current == currentsParent->right) {
		current = currentsParent;
		currentsParent = currentsParent->parent;
	}

	return currentsParent;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::min() {
	RedBlackTreeNodeImpl<T> * current = this;

	while (current->left != NULL)
		current = current->left;

	return current;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::min(T & cap) {
	RedBlackTreeNodeImpl<T> * current = this;
	RedBlackTreeNodeImpl<T> * result = NULL;

	while(current) {
		if (current->element == cap) {
			result = current;
			current = NULL;
		} else if (current->element < cap) {
			current = current->right;
		} else {
			result = current;
			current = current->left;
		}
	}

	return result;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::max() {
	RedBlackTreeNodeImpl<T> * current = this;

	while (current->right != NULL)
		current = current->right;

	return current;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::max(T & cap) {
	RedBlackTreeNodeImpl<T> * current = this;
	RedBlackTreeNodeImpl<T> * result = NULL;

	while(current) {
		if (current->element == cap) {
			result = current;
			current = NULL;
		} else if (current->element > cap) {
			current = current->left;
		} else {
			result = current;
			current = current->right;
		}
	}

	return result;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTreeNodeImpl<T>::search(T & toSearch) {
	RedBlackTreeNodeImpl<T> * current = this;

	while (current != NULL && current->element != toSearch) {
		current = (current->element < toSearch? current->right : current->left);
	}

	return current;
}

template<typename T>
RedBlackTreeColor RedBlackTreeNodeImpl<T>::getColor() {
	return color;
}

template<typename T>
T & RedBlackTreeNodeImpl<T>::getElement() {
	return element;
}

#endif