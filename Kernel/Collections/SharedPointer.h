#ifndef SHAREDPOINTER_H 
#define SHAREDPOINTER_H

#include "../Log/Log.h"
extern Log * plog;

template<typename T>
class SharedPointerContainer {
public:
	int refCount;
	T * pointer;

	SharedPointerContainer(T * pointer);
	~SharedPointerContainer();
	void operator++();
	void operator--();
};

template<typename T>
class SharedPointer {
private:
	SharedPointerContainer<T> * container;

public:
	SharedPointer();
	SharedPointer(T * pointer);
	SharedPointer(const SharedPointer<T> & copied);						// Copy constructor
	SharedPointer(SharedPointer<T> && moved);							// Move constructor
	SharedPointer<T> & operator=(const SharedPointer<T> & copied);		// Copy assignment operator
	SharedPointer<T> & operator=(SharedPointer<T> && moved);			// Move assignment operator
	virtual ~SharedPointer();
	T & operator*();
	T * operator->() const;
};

template<typename T>
SharedPointer<T>::SharedPointer() {
	container = new SharedPointerContainer<T>(nullptr);
}

template<typename T>
SharedPointer<T>::SharedPointer(T * pointer) {
	container = new SharedPointerContainer<T>(pointer);
}

template<typename T>
SharedPointer<T>::SharedPointer(const SharedPointer<T> & copied) {
	container = copied.container;
	++(*container);
}

template<typename T>
SharedPointer<T>::SharedPointer(SharedPointer<T> && moved) {
	container = moved.container;
}

template<typename T>
SharedPointer<T> & SharedPointer<T>::operator=(const SharedPointer<T> & copied) {
	--(*container);
	container = copied.container;
	++(*container);
}

template<typename T>
SharedPointer<T> & SharedPointer<T>::operator=(SharedPointer<T> && moved) {
	container = moved.container;
}

template<typename T>
SharedPointer<T>::~SharedPointer() {
	plog->str("before *").newline();
	SharedPointerContainer<T> & c = *container;
	plog->str("before --").newline();
	--c;
	plog->str("after --").newline();
}

template<typename T>
T & SharedPointer<T>::operator*() {
	return *container->pointer;
}

template<typename T>
T * SharedPointer<T>::operator->() const {
	return container->pointer;
}

template<typename T>
SharedPointerContainer<T>::SharedPointerContainer(T * pointer) {
	this->refCount = 1;
	this->pointer = pointer;
}

template<typename T>
SharedPointerContainer<T>::~SharedPointerContainer() {
/*	plog->str("before if").newline();
	if (pointer != nullptr) {
		plog->str("before delete").addr(pointer).newline();
		delete pointer;
		plog->str("after delete").newline();
	}
	plog->str("after if").newline();*/
}

template<typename T>
void SharedPointerContainer<T>::operator++() {
	++refCount;
}

template<typename T>
void SharedPointerContainer<T>::operator--() {
	plog->str("before if").newline();
	if (--refCount == 0) {
		plog->str("before delete").newline();

		if (pointer != nullptr) {
			plog->str("before delete").addr(pointer).newline();
			delete pointer;
			plog->str("after delete").newline();
		}

		plog->str("before delete").addr(this).newline();
		delete this;
		plog->str("after delete").newline();
	}
	plog->str("after if").newline();
}

#endif
