#ifndef LIST_H
#define LIST_H

#include "../Wyrm.h"

template<typename T>
class List {
private:
	class BaseNode {
	public:
		BaseNode * previous;
		BaseNode * next;

		BaseNode() {
			previous = nullptr;
			next = nullptr;
		}
	};

	class LastNode : public BaseNode {};

	class Node : public BaseNode {
	public:
		T element;

		Node(T & e) : element(e) {}
	};

	LastNode last;

	friend class Iterator;

public:
	class Iterator {
	private:
		BaseNode * last;
		BaseNode * current;

	public:
		Iterator();
		Iterator(List<T>::BaseNode * current, List<T>::BaseNode * last);
		Iterator & operator++();
		Iterator operator++(int unused);
		Iterator & operator--();
		Iterator operator--(int unused);
		Iterator & operator+=(int value);
		Iterator & operator-=(int value);
		Iterator operator+(int value);
		Iterator operator-(int value);
		bool operator==(const Iterator & other); 
		bool operator!=(const Iterator & other);
		T & operator*();
		bool atEnd();
		bool atBeginning();
		void insert(T & element);
		void remove();
	};

	List();
	Iterator begin();
	Iterator end();
	void pushFront(T & element);
	void pushBack(T & element);
	void popFront();
	void popBack();
	T & front();
	T & back();
	void clear();
	int count();
};

template<typename T>
List<T>::List() {
	last.next = &last;
	last.previous = &last;
}

template<typename T>
typename List<T>::Iterator List<T>::begin() {
	Iterator iterator(last.next, &last);
	return iterator;
}

template<typename T>
typename List<T>::Iterator List<T>::end() {
	Iterator iterator(&last, &last);
	return iterator;
}

template<typename T>
void List<T>::pushFront(T & element) {
	begin().insert(element);
}

template<typename T>
void List<T>::pushBack(T & element) {
	end().insert(element);
}

template<typename T>
void List<T>::popFront() {
	begin().remove();
}

template<typename T>
void List<T>::popBack() {
	(--end()).remove();
}

template<typename T>
T & List<T>::front() {
	return ((Node*)last.next)->element;
}

template<typename T>
T & List<T>::back() {
	return ((Node*)last.previous)->element;
}

template<typename T>
void List<T>::clear() {
	while(last.next != &last)
 		popBack();
}

template<typename T>
int List<T>::count() {
	int count = 0;

	for (Iterator i = begin(); i != end(); ++i)
		count++;

	return count;
}

template<typename T>
List<T>::Iterator::Iterator() {
	this->current = nullptr;
	this->last = nullptr;
}

template<typename T>
List<T>::Iterator::Iterator(List<T>::BaseNode * current, List<T>::BaseNode * last) {
	this->current = current;
	this->last = last;
}

template<typename T>
typename List<T>::Iterator List<T>::Iterator::operator++(int unused) {
	List<T>::Iterator iterator(operator++());
	return iterator;
}

template<typename T>
typename List<T>::Iterator & List<T>::Iterator::operator++() {
	current = current->next;
	return *this;
}

template<typename T>
typename List<T>::Iterator List<T>::Iterator::operator--(int unused) {
	List<T>::Iterator iterator(operator--());
	return iterator;
}

template<typename T>
typename List<T>::Iterator & List<T>::Iterator::operator--() {
	current = current->previous;
	return *this;
}

template<typename T>
typename List<T>::Iterator & List<T>::Iterator::operator+=(int value) {
	for (int i = 0; i < value; i++)
		(*this)++;

	return *this;
}

template<typename T>
typename List<T>::Iterator & List<T>::Iterator::operator-=(int value) {
	for (int i = 0; i < value; i++)
		(*this)--;

	return *this;
}

template<typename T>
typename List<T>::Iterator List<T>::Iterator::operator+(int value) {
	List<T>::Iterator iterator(*this);
	return iterator += value;
}

template<typename T>
typename List<T>::Iterator List<T>::Iterator::operator-(int value) {
	List<T>::Iterator iterator(*this);
	return iterator -= value;
}

template<typename T>
bool List<T>::Iterator::operator==(const List<T>::Iterator & other) {
	return current == other.current;
}

template<typename T>
bool List<T>::Iterator::operator!=(const List<T>::Iterator & other) {
	return !(*this == other);
}

template<typename T>
T & List<T>::Iterator::operator*() {
	return ((Node*)current)->element;
}

template<typename T>
bool List<T>::Iterator::atBeginning() {
	return current == last->next;
}

template<typename T>
bool List<T>::Iterator::atEnd() {
	return current == last;
}

template<typename T>
void List<T>::Iterator::insert(T & element) {
	List<T>::Node * newNode = new Node(element);
	newNode->previous = current->previous;
	newNode->next = current;
	current->previous->next = newNode;
	current->previous = newNode;
	current = newNode;
}

template<typename T>
void List<T>::Iterator::remove() {
	current->previous->next = current->next;
	current->next->previous = current->previous;
	List<T>::BaseNode * toDelete = current;
	current = current->next;
	delete toDelete;
}

#endif
