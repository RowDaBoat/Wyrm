#ifndef CHUNK_H
#define CHUNK_H

#include <new>
#include <stdint.h>

class ChunkAllocator;

class Chunk {
public:
	uint32_t objectCount;
	ChunkAllocator * allocator;

	Chunk(uint32_t objectCount, ChunkAllocator * allocator);
	
	bool operator<(Chunk & chunk);
	bool operator>(Chunk & chunk);
	bool operator<=(Chunk & chunk);
	bool operator>=(Chunk & chunk);
	bool operator==(Chunk & chunk);
	bool operator!=(Chunk & chunk);
};

#endif