#include <new>
#include "Chunk.h"
#include "ChunkAllocator.h"
#include "../StandardLibrary/mathutil.h"
#include "../Collections/RedBlackTree.h"
#include "../Collections/RedBlackTreeNode.h"
#include "../DebugLibrary/Debug.h"

ChunkAllocator::ChunkAllocator(PageAllocator & pa, uint32_t size, Log & l) : pageAllocator(pa), log(l) {
	userChunkSize = size;
	treeNodeSize = RedBlackTree<Chunk>::getNodeSize() + userChunkSize;
	pagesPerBlock = nextHighestPowerOf2(upperDivide(
		treeNodeSize * MinObjectsPerBlock, PageAllocator::PageSize
	));
	memoryStart = pageAllocator.managedMemoryStart();
	objectsPerBlock = (pagesPerBlock * PageAllocator::PageSize) / treeNodeSize;
}

uint64_t ChunkAllocator::getChunkSize() {
	return userChunkSize;
}

void * ChunkAllocator::alloc() {
	RedBlackTreeNode<Chunk> * root = freeChunks.getRoot();

	if (root == nullptr) {
		if ((root = createNode()) == nullptr)
			return nullptr;

		freeChunks.insert(root);
	}

	bool removeNode = false;
	void * chunk = allocateChunk(root, removeNode);

	if (removeNode)
		freeChunks.remove(root);

	return chunk;
}

void ChunkAllocator::logNode(Log & log, const char * name, RedBlackTreeNode<Chunk> * node) {
	log.str(name).str(" -> ").addr(node);

	if (node) {
		log.str("-").addr((char*)node + node->getElement().objectCount * treeNodeSize);
		log.str(" count:").count(node->getElement().objectCount);
	}

	log.newline();
}

void ChunkAllocator::dealloc(void * toFree) {
	RedBlackTreeNode<Chunk> * root = freeChunks.getRoot();
	RedBlackTreeNode<Chunk> * node = getNodeFromChunk(toFree);
	RedBlackTreeNode<Chunk> * prev = (root == nullptr? nullptr : root->max(node->getElement()));
	RedBlackTreeNode<Chunk> * next = (root == nullptr? nullptr : root->min(node->getElement()));

	//Re-initialize the node
	Chunk chunk(1, this);
	RedBlackTree<Chunk>::createNodeFromMemoryChunk(node, chunk);
	if (prev != nullptr && canMerge(prev->getElement(), node->getElement())) {
		freeChunks.remove(prev);
		merge(prev->getElement(), node->getElement());
		node = prev;
	}

	if (next != nullptr && canMerge(node->getElement(), next->getElement())) {
		freeChunks.remove(next);
		merge(node->getElement(), next->getElement());
	}

	if (node->getElement().objectCount == objectsPerBlock) {
		pageAllocator.dealloc(node);
	} else {
		freeChunks.insert(node);
	}
}

RedBlackTree<Chunk> & ChunkAllocator::getFreeChunksTree() {
	return freeChunks;
}

ChunkAllocator & ChunkAllocator::getAllocatorFromPointer(void * allocated) {
	return *(getNodeFromChunk(allocated)->getElement().allocator);
}

RedBlackTreeNode<Chunk> * ChunkAllocator::createNode() {
	char * page = (char*)pageAllocator.alloc(pagesPerBlock);

	if (page == nullptr)
		return nullptr;

	Chunk chunk(objectsPerBlock, this);
	return RedBlackTree<Chunk>::createNodeFromMemoryChunk(page, chunk);
}

void * ChunkAllocator::allocateChunk(RedBlackTreeNode<Chunk> * node, bool & removeNode) {
	Chunk & nodeChunk(node->getElement());
	nodeChunk.objectCount--;
	removeNode = (nodeChunk.objectCount == 0);

	//Create the node
	Chunk allocatedChunk(1, this);
	void * allocatedNodeAddress = (char*)node + nodeChunk.objectCount * treeNodeSize;
	RedBlackTreeNode<Chunk> * allocatedNode = RedBlackTree<Chunk>::createNodeFromMemoryChunk(
		allocatedNodeAddress, allocatedChunk
	);

	//allocatedNode + 1 skips the node to the free user chunk
	return (void*)(allocatedNode + 1);
}

RedBlackTreeNode<Chunk> * ChunkAllocator::getNodeFromChunk(void * chunk) {
	return (RedBlackTreeNode<Chunk>*)chunk - 1;
}

bool ChunkAllocator::canMerge(Chunk & chunkA, Chunk & chunkB) {
	//Check whether one of the nodes are full
	if (chunkA.objectCount == objectsPerBlock || chunkB.objectCount == objectsPerBlock) {
		return false;
	}

	//Check if both belong to the same block
	uint32_t blockSize = pagesPerBlock * PageAllocator::PageSize;
	uint64_t nodeIdForChunkA = ((char*)(&chunkA) - (char*)memoryStart) / blockSize;
	uint64_t nodeIdForChunkB = ((char*)(&chunkB) - (char*)memoryStart) / blockSize;

	if (nodeIdForChunkA != nodeIdForChunkB) {
		return false;
	}

	//Check if both are contiguous
	return (char*)&chunkA + chunkA.objectCount * treeNodeSize == (char*)&chunkB;
}

void ChunkAllocator::merge(Chunk & chunkA, Chunk & chunkB) {
	chunkA.objectCount += chunkB.objectCount;
}
