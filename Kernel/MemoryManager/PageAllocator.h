#ifndef PAGEALLOCATOR_H
#define PAGEALLOCATOR_H

#include <stdint.h>
#include "../Collections/BitArray.h"
#include "../Log/Log.h"

class PageAllocator {
public:
	static const uint64_t PageSize = 4096;
	static const uint64_t PageMask = ~0xFFF;

	PageAllocator(void * start, uint64_t size, Log & log);

	//Allocation methods
	void * alloc(uint64_t pageCount);
	void * alloc(void * address, uint64_t size);
	void dealloc(void * address);
	void blockSection(void * fromAddress, void * toAddress);

	//Additional information methods
	void * managedMemoryStart();
	uint64_t managedSize();
	uint64_t actualSize();
	void * getBuddyTableAddress();
	uint32_t getBuddyTablePageCount();

private:
	static const uint32_t PartialOrRequested = 0;
	static const uint32_t Complete = 1;
	static const uint32_t NodeSize = 2;

	uint32_t managedPageCount;
	uint8_t * buddyTable;
	uint32_t tableSize;
	void * start;
	uint64_t totalSize;
	Log & log;

	void * allocNode(uint32_t index, void * address, int nodePageCount, int requestedPageCount, bool & complete);
	void * allocNodeAtAddress(uint32_t index, void * requestedAddress, int requestedPageCount, void * currentAddress, int nodePageCount);
	void freeNode(uint32_t index, void * toFreeAddress, void * currentAddress, int nodePageCount);
	void blockSection(int index, int nodePageCount, void * currentAddress, void * fromAddress, void * toAddress);
};

#endif