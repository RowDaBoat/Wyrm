#ifndef PAGEALLOCATORTRANSACTION_H
#define PAGEALLOCATORTRANSACTION_H

#include <stdint.h>
#include "PageAllocator.h"

template<unsigned int T>
class PageAllocatorTransaction {
private:
	PageAllocator & pageAllocator;
	void * allocs[T];
	uint32_t count;

public:
	PageAllocatorTransaction(PageAllocator & pageAllocator);
	void * alloc(uint64_t pages);
	void fail();
	bool failWithFalse();
	void * failWithNullptr();
	void * get(uint32_t index);
};

template<unsigned int T>
PageAllocatorTransaction<T>::PageAllocatorTransaction(PageAllocator & pa) : pageAllocator(pa) {
	count = 0;

	for (int i = 0; i < T; i++)
		allocs[i] = nullptr;
}

template<unsigned int T>
void * PageAllocatorTransaction<T>::alloc(uint64_t pages) {
	return (allocs[count++] = pageAllocator.alloc(pages));
}

template<unsigned int T>
void PageAllocatorTransaction<T>::fail() {
	for(int i = 0; allocs[i] != nullptr; i++)
		pageAllocator.dealloc(allocs[i]);
}

template<unsigned int T>
void * PageAllocatorTransaction<T>::get(uint32_t index) {
	return allocs[index];
}

template<unsigned int T>
bool PageAllocatorTransaction<T>::failWithFalse() {
	fail();
	return false;
}

template<unsigned int T>
void * PageAllocatorTransaction<T>::failWithNullptr() {
	fail();
	return nullptr;
}

#endif
