#include <stdlib.h>
#include "../Log/log.h"
#include "MemoryMap.h"

//Pure64's memory map address
MemoryMapEntry * MemoryMap::originalMemoryMap((MemoryMapEntry*)0x4000);

inline int clamp(int min, int max, int value) {
	return
		value < min? min :
		value > max? max :
		value;
}

static const char * Types[] = {
	"Undefined",
	"Usable",
	"Reserved",
	"Reclaimable",
	"NVS memory",
	"Bad memory",
};

MemoryMap::MemoryMap(Log & l, void * kernelStack) : log(l) {
	log.str("Memory map:").newline();

	//Get the number of entries
	MemoryMapEntry * current = (MemoryMapEntry*)originalMemoryMap;
	entryCount = 0;

	while (!isBlank(current)) {
		current++;
		entryCount++;
	}

	//Sort usable and reclaimable first
	qsort(originalMemoryMap, entryCount, sizeof(MemoryMapEntry), (comparer)memoryMapEntryComparer);

	//Leave the kernel out of the map
	originalMemoryMap->size -= (uint8_t*)kernelStack - (uint8_t*)originalMemoryMap->address;
	originalMemoryMap->address = kernelStack;

	//Get the number of valid entries
	current = originalMemoryMap;
	entryCount = 0;

	//Log the memory map
	while (isValid(current)) {
		log.addr(current->address).chr(' ')
			.sizeMb(current->size).chr(' ')
			.str(Types[clamp(0, 5, current->type)]).chr(' ')
			.newline();
		current++;
		entryCount++;
	}

	log.newline();
};

MemoryMapEntry & MemoryMap::operator[](uint32_t index) {
	if (!(0 <= index && index < entryCount)) {
		//This is a bug in the kernel
		log.str("Index out of range on MemoryMap::operator[], this is a bug in the kernel.").newline();
		return *originalMemoryMap;
	}

	return *(originalMemoryMap + index);
}

uint32_t MemoryMap::count() {
	return entryCount;
}

void * MemoryMap::startAddress() {
	return originalMemoryMap->address;
}

uint64_t MemoryMap::totalMemory() {
	uint8_t * start = (uint8_t*)originalMemoryMap->address;
	uint8_t * lastEntryAddress = (uint8_t*)(originalMemoryMap + entryCount - 1)->address;
	uint64_t lastEntrySize = (originalMemoryMap + entryCount - 1)->size;

	return (uint64_t)(lastEntryAddress + lastEntrySize - start);
}

bool MemoryMap::isBlank(MemoryMapEntry * entry) {
	return entry->address == nullptr &&
		entry->size == 0 &&
		entry->type == 0 &&
		entry->extendedAttributes == 0;
}

bool MemoryMap::isValid(MemoryMapEntry * entry) {
	return entry->address != nullptr && entry->size > 0 &&
		(entry->type == 1 || entry->type == 3) &&
		entry->extendedAttributes == 1;
}

int32_t MemoryMap::memoryMapEntryComparer(MemoryMapEntry * e1, MemoryMapEntry * e2) {
	bool v1 = MemoryMap::isValid(e1);
	bool v2 = MemoryMap::isValid(e2);

	return
		v1 && v2? (uint8_t*)e1->address - (uint8_t*)e2->address :
		v1 && !v2? 1 :
		!v1 && v2? -1 :
		0;
}
