/* Generated automatically. */
static const char configuration_arguments[] = "../sources/gcc-4.8.2/configure --target=x86_64-elf --prefix=/home/arkanrow/opt/cross --disable-nls --enable-languages=c,c++ --without-headers";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
