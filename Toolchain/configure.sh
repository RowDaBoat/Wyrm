#!/bin/bash

echo "execute this script with like this with the two dots:"
echo "user@linux$ . ./configure.sh"

export PREFIX="$HOME/opt/cross"
export TARGET=x86_64-elf
export PATH="$PREFIX/bin:$PATH"

echo "environment variables exported"
