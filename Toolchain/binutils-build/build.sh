#!/bin/bash

../sources/binutils-2.24/configure --target=x86_64-elf --prefix="$PREFIX" --disable-nls --disable-werror
make
make install
